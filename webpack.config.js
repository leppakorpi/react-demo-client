'use strict';

const webpack = require('webpack');
const plugins = require('webpack-load-plugins')();
const template = require('html-webpack-template');
const path = require('path');

const CommonsPlugin = webpack.optimize.CommonsChunkPlugin;

let cssLoader;
const TARGET = process.env.npm_lifecycle_event;
const compassIncludes = 'includePaths[]=' + path.resolve(__dirname, './node_modules/compass-mixins/lib');
if (TARGET === 'start') {
  process.env.BABEL_ENV = 'development';
  cssLoader = { test: /\.s?css$/, loader: 'style!css?sourceMap!sass?sourceMap&' + compassIncludes };
} else {
  cssLoader = { test: /\.s?css$/, loader: plugins.extractText.extract('style', 'css?sourceMap!sass?' + compassIncludes) };
  if (!process.env.BABEL_ENV) {
    process.env.BABEL_ENV = 'production';
  }
}

const config = {
  entry: {
    app: './src/app.jsx',
    vendors: ['react', 'react-dom', 'react-router', 'font-awesome-webpack', 'mobx', 'mobx-react', 'axios', 'spin.js', 'react-youtube', 'youtube-node'],
  },
  resolve: {
    extensions: ['', '.js', '.jsx']
  },
  output: {
    path: './build',
    filename: '[name].bundle.js',
  },
  module: {
    loaders: [
      { test: /\.jsx?$/, exclude: /node_modules/, loader: 'babel' },
      { test: /\.json$/, loader: 'json' },
      // { test: /\.(jpg|jpeg|gif|png|ico)$/, exclude: /node_modules/, loader: 'file?name=img/[path][name].[ext]&context=./app/images' },
      cssLoader,
      { test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/, loader: 'url?limit=10000&minetype=application/font-woff' },
      { test: /\.(ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/, loader: 'file' },
    ]
  },
  plugins: [
    new plugins.clean(['build'], { root: process.cwd(), verbose: false }),
    new plugins.copy([{ from: 'static' }]),
    new CommonsPlugin({ names: ['vendors'], filename: '[name].js' }),
    new CommonsPlugin('loader', 'loader.js', Infinity),

    new plugins.html({
      inject: false, template, title: 'My App', appMountId: 'app',
      filename: 'index.html', chunks: ['loader', 'vendors', 'app'], favicon: './favicon.ico',
    }),
  ],
  node: { fs: 'empty'},
  devtool: 'source-map' // && 'inline-source-map',
};

if (TARGET === 'build') {
  config.plugins = config.plugins.concat([
    new plugins.extractText('[name].css'),
    new webpack.DefinePlugin({ 'process.env': {NODE_ENV: '"production"'} }),
    new webpack.optimize.UglifyJsPlugin({ compress: { warnings: false } }),
  ]);
  delete config.devtool;
}

module.exports = config;

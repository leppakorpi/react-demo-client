import React from 'react';
import ReactDOM from 'react-dom';
import { Router, Route, Link, IndexRoute, browserHistory } from 'react-router';

import LinkPage from './links/LinkPage';
import ProfilePage from './profile/ProfilePage';

require('font-awesome-webpack');
require('./style.scss');

class About extends React.Component {
  render() {
    return <div id="about"><h1>About</h1>React demo app</div>;
  }
}

class HeaderFooter extends React.Component {
  static propTypes = {
    children: React.PropTypes.element.isRequired
  }

  render() {
    return (
      <div className="main-container">
        <div id="navbar">
          Header
          <ul>
            <li><Link to={'/links'} activeClassName="active"><i/><span>Links</span></Link></li>
            <li><Link to={'/about'} activeClassName="active"><i/><span>About</span></Link></li>
            <li><Link to={'/profile'} activeClassName="active"><i/><span>Profile</span></Link></li>
          </ul>
        </div>
        <div id="middle-area">
          <div id="content" className="radial-gradient">
            { this.props.children }
          </div>
        </div>
        <div id="footer">
          Footer
        </div>
      </div>
    );
  }
}

const app2 = (
  <HeaderFooter>
    <About/>
  </HeaderFooter>
);

const app = (
  <Router history={browserHistory}>
    <Route path="/" component={HeaderFooter}>
      <IndexRoute component={LinkPage}/>
      <Route path="about" component={About}/>
      <Route path="links" component={LinkPage}/>
      <Route path="profile" component={ProfilePage}/>
    </Route>
  </Router>
);

ReactDOM.render(app, document.getElementById('app'));

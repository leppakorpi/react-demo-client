import React from 'react';

export default class Edit extends React.Component {

  static propTypes = {
    value: React.PropTypes.string.isRequired,
    onEdit: React.PropTypes.func
  }

  constructor(props) {
    super(props);

    this.state = {
      editing: false
    };
  }

  render() {
    const {className, value, ...props} = this.props;

    if (!this.state.editing) {
      return (
        <span className={className} onClick={this.onTitleClick}>
          {value}
        </span>
      );
    }

    return (<input
      type="text"
      className={className}
      autoFocus={true}
      defaultValue={value}
      onBlur={this.finishEdit}
      onKeyPress={this.checkEnter}
      />
    );
  }

  onTitleClick = (e) => {
    this.setState({editing: true});
  }

  checkEnter = (e) => {
    if (e.key === 'Enter') {
      this.finishEdit(e);
    }
  }
  finishEdit = (e) => {
    const value = e.target.value;
    if (this.props.onEdit) {
      this.props.onEdit(value);
    }
    this.setState({editing: false});
  }
}

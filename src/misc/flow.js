/* @flow */

function add(x: number, y: number) {
  return x + y;
}

let z: string = add(1, 1);

type Data = { name: string, values: Array<number> };
let obj: Data = {values: [1]};

function appendToData(data: Data, value: number): void {
  data.values.append(value);
  return 'response';
}
appendToData(obj, 2);

type MyFunction = ((a: number, b: string) => number);
let f: MyFunction = function (a: number, b: string) {
  return a.toString() + b;
};

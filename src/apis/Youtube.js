import YouTube from 'youtube-node';

const apiKey = 'AIzaSyCWG1ZV7PpGsQ2WjwPDfm9cNbrhVNkGNAY';
const youTube = new YouTube();

youTube.setKey(apiKey);

exports.parseYouTubeURL = function(text) {
  const re = /https?:\/\/(?:[0-9A-Z-]+\.)?(?:youtu\.be\/|youtube(?:-nocookie)?\.com\S*?[^\w\s-])([\w-]{11})(?=[^\w-]|$)(?![?=&+%\w.-]*(?:['"][^<>]*>|<\/a>))[?=&+%\w.-]*/ig;
  const match = re.exec(text);
  if (!match || match.length < 1) {
    return null;
  }
  return match[1];
  // '<a href="http://www.youtube.com/watch?v=$1">YouTube link: $1</a>');
};

exports.getYouTubeVideoInfo = function (videoId) {
  return new Promise(function (resolve, reject) {
    youTube.getById(videoId, function (error, result) {
      if (error) {
        console.log('Youtube API returned error:', error);
        return reject(error.message || error);
      }
      if (!result.items || !result.items[0] || !result.items[0].snippet) {
        console.log('Invalid response:', result);
        return reject('Invalid response');
      }

      // console.log(JSON.stringify(result, null, 2));
      const snippet = result.items[0].snippet;
      let { title, thumbnails: { default:
        {url: smallThumbnail}, medium: {url: mediumThumbnail}, high: {url: bigThumbnail}
      }} = snippet;
      // console.log('youtube info:', title, smallThumbnail, mediumThumbnail, bigThumbnail);
      return resolve({title, thumbnail: mediumThumbnail});
    });
  });
};

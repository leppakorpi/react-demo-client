import axios from 'axios';

const api = axios.create({
  baseURL: 'https://tonis.herokuapp.com/',
  timeout: 5000,
  headers: { 'X-Foo': 'bar' },
});

exports.loadAllLinks = function () {
  return api.get('links').then(function (response) {
    if (!Array.isArray(response.data)) {
      return Promise.reject('api response data is not an array');
    }
    console.log('Loaded %d links from server.', response.data.length);
    return response.data;
  });
};

exports.addLink = function (link) {
  // return slowFailure(8000);

  const json = {
    title: link.title,
    url: link.url,
    thumbnail: link.thumbnail
  };
  return api.post('links', json).then((result) => {
    const newId = result.data && result.data.id;
    if (!newId) {
      return Promise.reject('No id in response');
    }
    return newId;
  });
};

exports.updateTitle = function (link, title) {
  return api.put('links/' + link.id, { title });
};

exports.updateRating = function (link, stars) {
  return api.put('links/' + link.id, { stars });
};

exports.deleteLink = function (link) {
  return api.delete('links/' + link.id);
};

function slowFailure(timeout) {
  return new Promise(function (resolve, reject) {
    setTimeout(function () {
      reject('Test failure');
    }, timeout);
  });
}
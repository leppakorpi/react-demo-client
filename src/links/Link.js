import {observable} from 'mobx';

export default class Link {

  @observable id = '';
  @observable title = '';
  @observable url = '';
  @observable thumbnail = '';
  @observable stars = 0;

  constructor(id, title, url, thumbnail, stars = 0) {
    this.id = id;
    this.title = title;
    this.url = url;
    this.thumbnail = thumbnail;
    this.stars = stars;
  }

  static fromJSON(json) {
    let {id, title, url, thumbnail, stars} = json;
    let link = new Link(id, title, url, thumbnail, stars);
    return link;
  }
}

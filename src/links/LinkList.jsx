import React from 'react';
import Link from './Link';
import LinkRow from './LinkRow';
import AddLink from './AddLink';

require('./Link.scss');

export default class LinkList extends React.Component {

  static propTypes = {
    // links: React.PropTypes.arrayOf(React.PropTypes.instanceOf(Link)).isRequired
    links: React.PropTypes.object.isRequired, // links is mobX decorated array
    onSelect: React.PropTypes.func,
    onAdd: React.PropTypes.func,
    onDelete: React.PropTypes.func,
  }

  render() {
    return (
      <div className="link-list">
        <h3>Add New Link</h3>
        <AddLink onAdd={this.props.onAdd}/>
        <br/>
        <h3>Previous Posts</h3>
        {
          this.props.links.map( link => {
            return (
              <LinkRow key={link.id} link={link} onSelect={(event) => this.props.onSelect(link, event)}
                onDelete={(event) => this.props.onDelete(link, event)} />
            );
          })
        }
      </div>
    );
  }
}

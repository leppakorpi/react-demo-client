import React, {Component} from 'react';
import {observer} from 'mobx-react';
import YouTube from 'react-youtube';

import youtubeApi from '../apis/Youtube';
import Link from './Link';

import Editable from '../misc/Editable';

require('./Link.scss');

@observer
export default class LinkDetails extends Component {

  static propTypes = {
    link: React.PropTypes.instanceOf(Link),
    onEdit: React.PropTypes.func,
  }

  render() {
    if (!this.props.link) {
      return <div className="link-details">Nothing selected</div>;
    }

    let videoId = youtubeApi.parseYouTubeURL(this.props.link.url);
    const opts = {
      width: '100%',
      playerVars: { // https://developers.google.com/youtube/player_parameters
        autoplay: 0
      }
    };

    return (
      <div className="link-details">
        <Editable className="title" value={this.props.link.title} onEdit={this.props.onEdit} />
        <br/>
        <div className="videoContainer">
          <YouTube key={this.props.link.id} className="video" videoId={videoId} opts={opts}/>
        </div>
      </div>
    );
  }
}

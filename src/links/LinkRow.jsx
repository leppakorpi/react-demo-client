import React, {Component} from 'react';
import {observer} from 'mobx-react';
import {Klass as StarRatingInput, css as starCss } from 'react-star-rating-input';

import Link from './Link';
import LinkApi from '../apis/LinkApi';

require('style!react-star-rating-input/src/css.js');

@observer
export default class LinkRow extends Component {

  static propTypes= {
    link: React.PropTypes.instanceOf(Link).isRequired,
    onSelect: React.PropTypes.func,
    onDelete: React.PropTypes.func,
  }

  render() {
    return (
      <div className="row" onClick={this.props.onSelect}>
        <div className="img-holder">
          <img src={this.props.link.thumbnail}/>
        </div>
        <div>
          <span className="title">
            {this.props.link.title}
          </span>
          <br/><br/>
        </div>
        <div ref="delete" className="delete fa fa-trash-o fa-lg" onClick={this.props.onDelete}/>
        <br/>
      </div>
    );
  }
          /*
          <div onClick={this.noBubbling}>
            <StarRatingInput className="stars" showClear={false} value={this.props.link.stars || 1} onChange={this.onRatingChange} />
          </div>
          */

  onRatingChange = (value) => {
    const link = this.props.link;
    const oldRating = link.stars;
    // update UI optimistically
    link.stars = value;

    LinkApi.updateRating(link, value).then( (result) => {
      console.log('Link rating updated:', result.data.stars);
    }).catch( (error) => {
      console.error('Rating update failed:', error);
      link.stars = oldRating;
    });
  }

  noBubbling = (event) => {
    event.stopPropagation();
  }
}

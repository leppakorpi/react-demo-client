import React from 'react';
import Spinner from 'spin.js';
import Link from './Link';

export default class AddLink extends React.Component {

  static propTypes = {
    onAdd: React.PropTypes.func,
  }

  constructor(props) {
    super(props);
    this.state = {
      errorMsg: null,
    };
  }

  render() {
    return (
      <div className="addLink" ref="div">
        <input placeholder="paste YouTube link here"
          ref="input"
          onKeyPress={this.checkEnter}
        />
        <br/>
        <span className="error" hidden={!this.state.errorMsg} >
          {this.state.errorMsg}
        </span>
      </div>
    );
  }

  checkEnter = (e) => {
    if (e.key === 'Enter') {
      this.addLink();
    }
  }

  addLink() {
    this.setState({ errorMsg: null });
    this.refs.input.disabled = true;
    this.spinner = new Spinner({ scale: 0.5, position: 'relative', top: '-10px' }).spin();
    this.refs.div.appendChild(this.spinner.el);

    const url = this.refs.input.value;
    this.props.onAdd(url).then(() => {
      this.refs.input.value = '';
    }, (error) => {
      console.log('Failed to load video info:', error);
      this.setState({ errorMsg: error.toString() });
    }).then(() => {
      this.removeActivityIndicator();
    });
  }

  removeActivityIndicator = () => {
    this.refs.input.disabled = false;
    if (this.spinner) {
      this.refs.div.removeChild(this.spinner.el);
      this.spinner = null;
    }
  }
}

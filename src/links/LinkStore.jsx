import {observable} from 'mobx';

export default class LinkStore {
  @observable links = [];

  constructor() {
    this.links = [];
  }

  getAll() {
    return this.links;
  }

  addLink(link) {
    this.links.unshift(link);
  }

  removeLink(link) {
    const index = this.links.indexOf(link);
    if (index > -1) {
      this.links.splice(index, 1);
    }
  }

  replaceWith(links) {
    this.links = links;
    /*
    var newLinks = this.links.filter( link => {
      
    });
    */
  }

}

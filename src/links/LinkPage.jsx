import React from 'react';
import Spinner from 'spin.js';
require('regenerator-runtime/runtime');

import Link from './Link';
import LinkStore from './LinkStore';
import LinkList from './LinkList';
import LinkDetails from './LinkDetails';
import LinkApi from '../apis/LinkApi';
import * as youtube from '../apis/Youtube';

export default class LinkPage extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      store: new LinkStore(),
      selectedLink: null,
      loading: true,
    };
  }

  componentWillMount() {
    LinkApi.loadAllLinks().then( (data) => {
      const links = data.map(obj => Link.fromJSON(obj));
      this.state.store.replaceWith(links);
      this.setState({ loading: false, selectedLink: links[0] });
    }).catch((error) => {
      console.error('Failed to load links:', error);
    });
  }

  render() {
    if (this.state.loading) {
      return (
        <div className="app">
          <div className="link-list" ref={ (elem) => {
            if (!elem) { return; }
            const spinner = new Spinner({ scale: 0.5, position: 'relative', top: '-10px' }).spin();
            elem.appendChild(spinner.el);
          }}>
            Loading...
          </div>
          <LinkDetails link={null} />
        </div>
      );
    }

    const link = this.state.selectedLink;

    return (
      <div className="app">
        <LinkList links={ this.state.store.getAll() } onSelect={this.selectLink} onAdd={this.addLink} onDelete={this.deleteLink}/>
        <LinkDetails link={link} onEdit={(newTitle) => {this.editTitle(link, newTitle);}} />
      </div>
    );
  }

  selectLink = (link, event) => {
    this.setState({ selectedLink: link });
  }

  addLink = async (url) => {
    let videoId = youtube.parseYouTubeURL(url);
    if (!videoId) {
      return Promise.reject('Invalid URL');
    }
    console.log('id', videoId);

    const videoInfo = await youtube.getYouTubeVideoInfo(videoId);
    console.log('Got video info:', videoInfo);

    const id = new Date().getTime();
    const newLink = new Link(id, videoInfo.title, url, videoInfo.thumbnail);

    // Don't wait our own rest api, update UI optimistically
    this.state.store.addLink(newLink);
    this.selectLink(newLink);

    await LinkApi.addLink(newLink).then((newId) => {
      newLink.id = newId;
    }).catch((error) => {
      this.state.store.removeLink(newLink);
      this.forceUpdate();
      return Promise.reject('Failed to add link: ' + error);
    });

    return true;
  }

  deleteLink = (link, event) => {
    event.stopPropagation();
    LinkApi.deleteLink(link).then( (result) => {
      this.state.store.removeLink(link);
      this.forceUpdate();
    }).catch((error) => {
      console.error('Failed to remove link:', error);
    });
  }

  editTitle = (link, newTitle) => {
    const oldTitle = link.title;
    // update UI optimistically
    link.title = newTitle;

    LinkApi.updateTitle(link, newTitle).then( (result) => {
      console.log('Link title updated:', result.data.title);
    }).catch( (error) => {
      console.error('Title update failed:', error);
      link.title = oldTitle;
    });
  }
}
